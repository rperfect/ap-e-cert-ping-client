# AP E-Cert Ping Client

This is a .Net WCF client for the AP E-Cert SOAP interface. It contains sample code of how to connect to the AP E-cert web service and execute the "ping" SOAP method.

Download and unzip the APE WCF 4.5 Ping Client.zip file above.

